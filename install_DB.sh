HOSTNAME=$1
key=~/.ssh/shannon-AWS-key.pem  

if [ -z $HOSTNAME ]
then
    read "you need to add an IP address"
fi

# To ssh into AWS machine
ssh -o StrictHostKeyChecking=no -i $key  ec2-user@$HOSTNAME ' 

# Update the local repositories.
sudo yum update -y

# Install mariadb on the vm
sudo yum install mariadb-server -y

# Install instructions on wget
sudo yum install wget
wget https://raw.githubusercontent.com/cwoodruff/ChinookDatabase/master/Scripts/Chinook_MySql.sql

mysql -u root --password="" -e "source Chinook_MySql.sql"
mysql -u root --password="" -e "use Chinook; show tables;"
'

