# Installation of Chinook on a Amazon Linux 2 machine

This repository is a step by step guide on how to run your bash script from your local machine and run commands to SSH into the machine and download MariaDB so we can run Chinook.

### Pre Requisities

- You will need an AWS account
- Amazon Linux 2 machine running on AWS
- Key pair to connect to the AWS instance
- Text editor to write your bash script.


--------------------------------------------------------------------

#### Step 1: